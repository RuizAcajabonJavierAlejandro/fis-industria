```mermaid
graph TB

  

  subgraph "INDUSTRIA 4.0"
  Node1[INDUSTRIA 4.0] --> Node2[Descrito como fabrica inteligente según el gobierno Aleman]
  Node2 --> SubGraph1[BENEFICIOS]
  Node2 --> SubGraph2[CARACTERISTICAS]
  Node2 --> SubGraph3[SECTORES ESTRATEGICOS]
  SubGraph1 --> SubGraph4[--Capacidad de adaptación--]
  SubGraph4 --> SubGraph6[1._ Servir al cliente de forma más personalizada]
  SubGraph6 --> SubGraph7[2._ Diseñar, producir y vender en menos tiempo]
  SubGraph7 --> SubGraph8[3._ Crear series de producción más cortas y rentables]
  SubGraph2 --> SubGraph9[1._ Previsiones y digilización]
  SubGraph9 --> SubGraph10[2._ Reducción de costos y maximización de beneficios]
  SubGraph10 --> SubGraph11[3._ La personalización como eje para avanzar]
  SubGraph11 --> SubGraph12[4._ Importancia de los datos y la transparencia]
  SubGraph3 --> SubGraph13[--Electrónica--]
  SubGraph13 --> SubGraph14[--Sanidad--]
  SubGraph14 --> SubGraph15[--Logística--]
  SubGraph15 --> SubGraph16[--Ingeniería y construcción--]
  SubGraph16 --> SubGraph17[--Comercio y bancaria--]
end
```
```mermaid
graph TB

  

  subgraph "MÉXICO EN LA INDUSTRIA 4.0"
  Node1[MÉXICO EN LA INDUSTRIA 4.0] --> Node2[En México, esta revolución ha ido avanzando lentamente debido a que la manufactura ya es competitiva por los bajos costos de la mano de obra, <br> pero, inevitablemente, llegará el momento en que disminuya el precio de las tecnologías digitales y comience a extenderse su implementación.]
  Node2 --> SubGraph1[Para poder avanzar hacia la industria 4.0 <br> es necesario trabajar en cuatro pilares fundamentales.]
  Node2 --> SubGraph2[En el país se debe fundamentar principalmente <br> en cuatro ecosistemas para aprovechar las oportunidades de negocio:]
  Node2 --> SubGraph3[Tendencias más destacadas para continuar desarrollando la Industria 4.0]
  SubGraph1 --> SubGraph4[--Desarrollo de capital humano]
  SubGraph4 --> SubGraph6[--Innovación]
  SubGraph6 --> SubGraph7[--Clústers]
  SubGraph7 --> SubGraph8[--Adopción de tecnología]
  SubGraph2 --> SubGraph9[1._ Ecosistema de soluciones al cliente. ]
  SubGraph9 --> SubGraph10[2._Ecosistema de operaciones.]
  SubGraph10 --> SubGraph11[3._ Ecosistema de la tecnología.]
  SubGraph11 --> SubGraph12[4._ Ecosistema de las personas.]
  SubGraph3 --> SubGraph13[1. Suministro automático de materiales y productos <br> a través de la automatización de la planta de producción.]
  SubGraph13 --> SubGraph14[2. Costos de implementación más bajos gracias a la estandarización.]
  SubGraph14 --> SubGraph15[3. Arquitectura Cloud-Edge para reducir costos.]
end
```
  


